# ---------------------------------------
# Data overlapping stations
# ---------------------------------------

# ---------------------------------------
# db link
# ---------------------------------------
library(RSQLite)
SSGB.db = dbConnect(SQLite(), dbname="~/Cloud/Michael/Uni_temp/SSGB/SSGB.sqlite")

# ---------------------------------------
# Three stations with observations summed to month
# ---------------------------------------
# Split by high and low
# Create temp table
dbSendQuery(SSGB.db, "
   CREATE temp TABLE step AS
   SELECT HydroYear, strftime('%m', Date) AS Month, Station, SnowlineElev FROM SSGB_obs
   WHERE
      strftime('%m', Date) BETWEEN '01' AND '05' AND SnowlineElev != 'n' AND SnowlineElev != '99'
      OR strftime('%m', Date) BETWEEN '10' AND '12'  AND SnowlineElev != 'n' AND SnowlineElev != '99'
")

# Snow at each elevation
SSGB = dbGetQuery(SSGB.db, "
   SELECT HydroYear, Month, Station, SnowlineElev, COUNT(SnowlineElev) AS Days FROM step
   WHERE SnowlineElev != 'm'
   AND (Station = 'Glengyle' OR Station = 'LochArklet' OR Station = 'Stronachlachar')
   GROUP BY HydroYear, Month, Station, SnowlineElev
   ORDER BY Station, HydroYear, Month, SnowlineElev
")

# LOS for three stations
LOS = dbGetQuery(SSGB.db, "
   SELECT * FROM LOS
      WHERE KeyName = 'Glengyle'
      OR KeyName = 'LochArklet'
      OR KeyName = 'Stronachlachar'
")

LOS = LOS[LOS$Area<10,]

# Aspect for three stations
Asp = dbGetQuery(SSGB.db, "
   SELECT * FROM LOS_aspect
      WHERE KeyName = 'Glengyle'
      OR KeyName = 'LochArklet'
      OR KeyName = 'Stronachlachar'
")

# Clean up db
dbSendQuery(SSGB.db, "DROP TABLE step")
dbDisconnect(SSGB.db)
rm(SSGB.db)
