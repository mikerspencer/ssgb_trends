library(RSQLite)
library(ggplot2)

# Read Derry Lodge data
SSGB.db = dbConnect("SQLite", "~/Cloud/Michael/Uni_temp/SSGB/SSGB.sqlite")

# Create temp table
dbSendQuery(SSGB.db, "
            CREATE temp TABLE step AS
            SELECT HydroYear, Date, Snowline FROM SSGB_obs_elev
            WHERE
            strftime('%m', Date) BETWEEN '01' AND '05' AND Snowline != 'n' AND Snowline != '99' AND Snowline != 'm' AND Station='DerryLodge'
            OR strftime('%m', Date) BETWEEN '10' AND '12'  AND Snowline != 'n' AND Snowline != '99' AND Snowline != 'm' AND Station='DerryLodge'
            ")
# Snow at any elevation (high)
DL.high = dbGetQuery(SSGB.db, "
   SELECT HydroYear, strftime('%Y-%m', Date) AS Month, COUNT(Snowline) AS Days FROM step
   GROUP BY Month
   ORDER BY Month
")

# 450 m snowline (low)
DL.low = dbGetQuery(SSGB.db, "
   SELECT HydroYear, strftime('%Y-%m', Date) AS Month, COUNT(Snowline) AS Days FROM step
   WHERE Snowline != '600'
      AND Snowline != '750'
      AND Snowline != '900'
      AND Snowline != '1050'
      AND Snowline != '1200'
   GROUP BY Month
   ORDER BY Month
")
# Clean up db
dbSendQuery(SSGB.db, "DROP TABLE step")
dbDisconnect(SSGB.db)
rm(SSGB.db)

# Make month into Date
tidy = function(i){
   i$Date = as.Date(paste0(i$Month, "-01"))
   i$Month = as.numeric(substr(i$Month, 6, 7))
   i$Month = factor(i$Month, labels=month.abb[c(1:5,10:12)])
   i$Month = factor(i$Month, levels=month.abb[c(10:12,1:5)])
   i
}

DL.high = tidy(DL.high)
DL.low = tidy(DL.low)

# Plot as monthly box plots
png("~/Cloud/Michael/Uni_temp/blog/DerryLodge_boxes.png", height=800, width=600)
par(mfrow=c(2,1))
boxplot(Days ~ Month, DL.high, col="darkorange", lwd=1.3, xlab="Month of year", ylab="Days snow cover per month", main="Snow cover at high elevations")
boxplot(Days ~ Month, DL.low, col="purple3", lwd=1.3, xlab="Month of year", ylab="Days snow cover per month", main="Snow cover at low elevations")
dev.off()

# Faceted scatters
DL.high$elev = "High"
DL.low$elev = "Low"
x = rbind(DL.high, DL.low)
sp = ggplot(x, aes(x=HydroYear, y=Days)) +
   geom_point(shape=1) +
   stat_smooth(data = subset(x, elev=="High"), colour="darkorange", size=1.2) +
   stat_smooth(data = subset(x, elev=="Low"), colour="purple3", size=1.2)
# Set text size
s = 14
# Write to png
png("~/Cloud/Michael/Uni_temp/blog/DerryLodge_bymonth.png", height=1000, width=900)
# facet scatter plots   
sp +
   facet_grid(Month ~ elev, scales="free_y") +
   xlab("Winter beginning in year") +
   ylab("Days snow cover per month") +
   theme(title = element_text(size = s), strip.text = element_text(size = s), axis.text = element_text(size = s))
dev.off()

# Plot as monthly box plots (ggplot)
png("~/Cloud/Michael/Uni_temp/blog/DerryLodge_boxes_gg.png", height=800, width=600)
p = ggplot(x, aes(factor(Month), Days)) +
   geom_boxplot(data = subset(x, elev=="High"), colour="darkorange", fill="grey80") +
   geom_boxplot(data = subset(x, elev=="Low"), colour="purple3", fill="grey80") +
   geom_jitter(shape=1, size=0.5)
p +
   facet_grid(elev ~ ., scales="free_y") +
   ylab("Days snow cover per month") +
   xlab("") +
   theme(title = element_text(size = s), strip.text = element_text(size = s), axis.text = element_text(size = s))
dev.off()